package sh.morph.utils

import com.google.common.primitives.{Floats, Ints, Longs}

/** ****
  *
  * @author Andrey Demin <ad@semantic-hub.com>
  *
  * ****/
object Nums {
  def toInt(s: String): Option[Int] = {
    Option(Ints.tryParse(s))
  }

  def toLong(s: String): Option[Long] = {
    Option(Longs.tryParse(s))
  }

  def toFloat(s: String): Option[Float] = {
    Option(Floats.tryParse(s))
  }
}
