package sh

/** ****
  *
  * @author Andrey Demin <ad@semantic-hub.com>
  *
  * ****/
package object morph {

  type ErrorMessage = String

  // Exceptions
  class InitializeException(msg: String) extends Exception(msg)

  class RedundantInitializationException(msg: String) extends Exception(msg)
}
