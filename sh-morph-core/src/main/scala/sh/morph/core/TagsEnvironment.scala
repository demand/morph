package sh.morph.core

import sh.morph.ContextRow
import sh.morph.core.TagsEnvironment.{MTag, MTagCategory}

/** ****
  *
  * @author Andrey Demin <ad@semantic-hub.com>
  *
  * ****/
object TagsEnvironment {
  case class MTagCategory(name: String, tags: Seq[MTag])
  case class MTag(name: String, category: MTagCategory)
}

class TagsEnvironment {
  private[this] var categories: Seq[MTagCategory] = Seq.empty

  private[this] var normalizedTags: Map[String, MTag] = Map.empty

  def fromContextRows(root: ContextRow): Unit = {

  }

  def resolve(s: String): Option[MTag] = {
    normalizedTags.get(normTagName(s))
  }

  private[this] def normTagName(s: String): String =
    s.trim.toLowerCase
}
