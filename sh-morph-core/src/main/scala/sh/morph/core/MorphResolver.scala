package sh.morph.core

import java.io.{BufferedInputStream, File, FileInputStream, InputStream}
import java.util.zip.GZIPInputStream

import sh.morph.{InitializeException, RedundantInitializationException}

import scala.io.Source

/** ****
  *
  * @author Andrey Demin <ad@semantic-hub.com>
  *
  * ****/

object MorphResolver {

  class MorphResolverBuilder {
    private[this] var dictInput: Option[InputStream] = None
    private[this] var rulesInput: Option[InputStream] = None

    @throws(classOf[RedundantInitializationException])
    private[this] def checkDictField(): Unit = {
      if (dictInput.nonEmpty) throw new RedundantInitializationException("Dict field is already initialized")
    }

    @throws(classOf[RedundantInitializationException])
    private[this] def checkRulesField(): Unit = {
      if (rulesInput.nonEmpty) throw new RedundantInitializationException("Rules field is already initialized")
    }

    @throws(classOf[RedundantInitializationException])
    def withDictResource(resourcePath: String): MorphResolverBuilder = {
      val resStream = classOf[MorphResolver].getClassLoader.getResourceAsStream(resourcePath)
      if (resourcePath.endsWith(".gz")) {
        withDictGZStream(resStream)
      } else {
        withDictStream(resStream)
      }
    }

    @throws(classOf[RedundantInitializationException])
    def withDictStream(stream: InputStream): MorphResolverBuilder = {
      checkDictField()
      dictInput = Some(stream)
      this
    }

    @throws(classOf[RedundantInitializationException])
    def withDictGZStream(stream: InputStream): MorphResolverBuilder = {
      withDictStream(new GZIPInputStream(new BufferedInputStream(stream)))
    }

    @throws(classOf[RedundantInitializationException])
    def withDictFile(file: File): MorphResolverBuilder = {
      val fileStream = new FileInputStream(file)
      if (file.getName.endsWith(".gz")) {
        withDictGZStream(fileStream)
      } else {
        withDictStream(fileStream)
      }
    }

    @throws(classOf[RedundantInitializationException])
    def withRulesResource(resourcePath: String): MorphResolverBuilder = {
      val resStream = classOf[MorphResolver].getClassLoader.getResourceAsStream(resourcePath)
      if (resourcePath.endsWith(".gz")) {
        withRuesGZStream(resStream)
      } else {
        withRulesStream(resStream)
      }
    }

    @throws(classOf[RedundantInitializationException])
    def withRulesStream(stream: InputStream): MorphResolverBuilder = {
      checkRulesField()
      rulesInput = Some(stream)
      this
    }

    @throws(classOf[RedundantInitializationException])
    def withRuesGZStream(stream: InputStream): MorphResolverBuilder = {
      withRulesStream(new GZIPInputStream(new BufferedInputStream(stream)))
    }

    @throws(classOf[RedundantInitializationException])
    def withRulesFile(file: File): Unit = {
      val fileStream = new FileInputStream(file)
      if (file.getName.endsWith(".gz")) {
        withRuesGZStream(fileStream)
      } else {
        withRulesStream(fileStream)
      }
    }

    @throws(classOf[InitializeException])
    def build: MorphResolver = {
      (dictInput, rulesInput) match {
        case (Some(ds), Some(rs)) => new MorphResolver(ds, rs)
        case _ => throw new InitializeException("Uninitialized fields for builder")
      }
    }
  }

  def newBuilder: MorphResolverBuilder = new MorphResolverBuilder

  def main(args: Array[String]): Unit = {
    val resolver = newBuilder
      .withDictResource("input/zal-dict-utf8.txt.gz")
//      .withDictFile(new File("./sh-morph-core/src/main/resources/input/zdict-utf8.txt.gz"))
      .withRulesResource("input/rules.txt")
      .build

  }
}

class MorphResolver(dictInput: InputStream, rulesInput: InputStream) {

  private[this] var dict: Map[String, String] = Map.empty

  private[this] def readDict(): Unit = {
    val t = System.currentTimeMillis()

    val dictSource = Source.fromInputStream(dictInput)

    dictSource.getLines().foreach { line =>
      val items = line.split("\\s", 2)
      if (items.size > 1) {
        dict += (items(0) -> items(1))
      }
    }

    val tt = System.currentTimeMillis() - t

    println(s": ${dict.size} in ${tt}ms")

  }

  private[this] def readRules(): Unit = {

  }

  locally {
    readDict()
    readRules()
  }

}
