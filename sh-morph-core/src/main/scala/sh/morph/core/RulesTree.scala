package sh.morph.core

import sh.morph.core.TagsEnvironment.MTag

/** ****
  *
  * @author Andrey Demin <ad@semantic-hub.com>
  *
  * ****/
object RulesTree {

  class TransformNode(val transformStr: String, val tags: Seq[MTag])

  class BaseFormLeaf(val template: String)

  class CrumbNode(val letters: Seq[Char], val trsNode: TransformNode)

  class WalkState(val wordStub: String, val crumbNode: CrumbNode)

}
