package sh.morph.core

import java.io.File

import org.slf4j.LoggerFactory
import sh.morph.ContextRow
import sh.morph.core.RulesTree.BaseFormLeaf
import sh.morph.utils.Nums

import scala.annotation.tailrec
import scala.io.Source
import scala.util.{Failure, Success, Try}
import scala.util.control.Breaks._


/** ****
  *
  * @author Andrey Demin <ad@semantic-hub.com>
  *
  * ****/
object RulesParser {

  private val log = LoggerFactory.getLogger(this.getClass)

  object Indented {
    def parse(source: String, tab: Int): Indented = {
      val spanned = source.replaceAll("\t", " " * tab).span(' '.==)
      Indented(spanned._2, indent = spanned._1.length)
    }

    def apply(s: String, indent: Int): Indented = new Indented(s, indent)
  }

  class Indented(_text: String, val indent: Int) {
    val text: String = _text.trim

    def isEmpty: Boolean = text.isEmpty

    def isComment: Boolean = text.startsWith("#")

    def hasData: Boolean = !isEmpty && !isComment
  }

  private val DecommentRe = "^([^#]*).*".r

  def decomment(s: String): String =
    DecommentRe.findFirstMatchIn(s).map(_.group(1)).getOrElse("")

  private val defaultTabSize = 4
  private val minIndent = 2

  val headerEntriesNames = Vector("tab", "author", "version")

  case class Header(
    tab: Int = defaultTabSize,
    author: Option[String] = None,
    version: Option[String] = None)

  object CommandEntry {

    private val CommandEntryRe = "^\\s*@([\\w0-9-_.$]+)(?:\\s+(.+))?".r

    def parse(s: String): Option[CommandEntry] =
      CommandEntryRe.findFirstMatchIn(s).map(m => CommandEntry(m.group(1), Option(m.group(2))))
  }

  case class CommandEntry(cmd: String, value: Option[String])

  case class Rows(rowsRoot: ContextRow, header: Header = Header(), source: RulesInput)

  object SeparatedRows {
    def empty: SeparatedRows = new SeparatedRows(header = Vector.empty, body = Vector.empty)
  }

  case class SeparatedRows(header: Seq[CommandEntry], body: Seq[String])

  case class RowErr(source: RulesInput, rowNo: Int, msg: String)

  case class ContextBack(contextRow: Option[ContextRow], popupRow: Option[Indented])

  def readRows(source: RulesInput, tab: Int = defaultTabSize): Either[RowErr, Rows] = {

    val rowsR = Try {
      Source.fromInputStream(source.inputStream()).getLines().toVector
    } match {
      case Success(rows) => Right(rows)
      case Failure(_) => Left(RowErr(source, -1, s"Error reading ${source.srcType} '${source.name}'"))
    }

    val rowsRes = rowsR
      .right
      .flatMap { rawRows =>
        splitRows(source, rawRows.zipWithIndex.iterator, Right(SeparatedRows.empty))
      }
      .right
      .flatMap(convertToContextRows(source, _))
      .right
      .flatMap { rows =>
        val rootChildren = rows.rowsRoot.chidren

        val newChildren = rootChildren.foldLeft(Vector.empty[ContextRow]) { (collected, cur) =>

          val commandOpt = CommandEntry.parse(cur.text)
          commandOpt match {
            case Some(CommandEntry("import", text)) =>
              text.map(s => decomment(s).trim) match {
                case Some(t) =>
                  val newInp = source.relative(t)
                  val imported = readRows(newInp, rows.header.tab)

                  val coll = imported match {
                    case Right(impRr) =>
                      collected ++ impRr.rowsRoot.chidren
                    case _ =>
                      // TODO add error processing: Left(...)
                      collected
                  }

                  coll
                case _ =>
                  // TODO add error processing: Left(void input name)
                  collected
              }
            case _ => collected :+ cur
          }
        }

        Right(rows.copy(rowsRoot = ContextRow("", newChildren)))
      }

    rowsRes
  }


  @tailrec
  def splitRows(source: RulesInput, iter: Iterator[(String, Int)], state: Either[RowErr, SeparatedRows]):
  Either[RowErr, SeparatedRows] = {
    state match {
      case Right(sepr) =>
        if (iter.hasNext) {
          val (s, index) = iter.next()
          val decommented = decomment(s)
          val nextState =
            if (decommented.trim.isEmpty) {
              state
            } else {
              CommandEntry.parse(decommented) match {
                case Some(command) if headerEntriesNames.contains(command.cmd) =>
                  if (sepr.body.nonEmpty) {
                    Left(RowErr(source, index + 1, s"Command must be placed before data rows"))
                  } else {
                    Right(sepr.copy(header = sepr.header :+ command))
                  }
                case _ =>
                  Right(sepr.copy(body = sepr.body :+ decommented))
              }
            }
          if (nextState.isRight)
            splitRows(source, iter, nextState)
          else
            nextState
        } else {
          state
        }
      case _ => state
    }
  }

  private[this] def convertToContextRows(source: RulesInput, separatedRows: SeparatedRows,
    parentTabSize: Int = defaultTabSize): Either[RowErr, Rows] = {

    val tab = separatedRows.header
      .find(_.cmd == "tab")
      .flatMap(c => c.value)
      .flatMap(v => Nums.toInt(v.trim))
      .getOrElse(parentTabSize)
      .max(1).min(16)

    val header = Header(tab)
    val rowsRes = collectContextRows(None, tab, separatedRows.body.iterator)

    rowsRes.popupRow match {
      case Some(cr) =>
        Left(RowErr(source, -1, s"Parse not finished. With popupRow '${cr.text}' in ${source.srcType} '${source.name}'"))
      case _ =>
        val rows = Rows(rowsRes.contextRow.getOrElse(ContextRow("", Seq.empty)), header, source)
        Right(rows)
    }
  }

  private[this] def collectContextRows(row: Option[Indented], tab: Int,
    rowsIter: Iterator[String]): ContextBack = {

    val chb = Vector.newBuilder[ContextRow]

    def isChildrenRow(ir: Indented): Boolean = row.forall(_.indent < ir.indent)

    def nextIndented(): Option[Indented] = {
      if (rowsIter.hasNext) Option(Indented.parse(rowsIter.next(), tab))
      else None
    }

    var iro = nextIndented()

    breakable {
      while (iro.nonEmpty) {
        val ir = iro.get
        if (ir.hasData) {
          if (isChildrenRow(ir)) {
            val collected = collectContextRows(iro, tab, rowsIter)
            collected.contextRow.foreach(chb.+=)
            iro = collected.popupRow
          } else {
            break
          }
        } else {
          iro = nextIndented()
        }
      }
    }

    val children = chb.result()
    val ctxo = row.fold(Option(ContextRow("", children))) { r =>
        if (r.text.nonEmpty || children.nonEmpty) {
          Some(ContextRow(r.text, children))
        } else {
          None
        }
      }

    ContextBack(ctxo, iro)
  }

  def apply(mainFile: File): Option[RulesParser] = {
    None
  }

  def apply(mainInput: RulesInput): Option[RulesParser] = {

    val rowsResult = readRows(mainInput)

    None
  }

  def main(args: Array[String]): Unit = {

    val rows = readRows(new RulesInput.ResourceInput("input/zal-rules-main.list"))

    rows match {
      case Right(r) => println("Ok")
      case Left(err) => println(s"Row ${err.rowNo}: ${err.msg}")
    }

    println("xx")
  }
}

class RulesParser(val rulesRoot: ContextRow) {

  def rulesTree: Seq[BaseFormLeaf] = {
    Seq.empty
  }
}
