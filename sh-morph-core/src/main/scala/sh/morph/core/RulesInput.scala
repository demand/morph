package sh.morph.core

import java.io.{File, FileInputStream, InputStream}
import java.net.URI

import org.slf4j.LoggerFactory


/** ****
  *
  * @author Andrey Demin <ad@semantic-hub.com>
  *
  * ****/

object RulesInput {

  private[this] val log = LoggerFactory.getLogger(getClass)

  class FileInput(fromFile: File) extends RulesInput {

    private val file = fromFile.getCanonicalFile

    override val srcType: String = "file"

    override def name: String = file.getPath

    override def relative(path: String): RulesInput =
      new FileInput(new File(file.getParent, path))

    override def inputStream(): InputStream =
      new FileInputStream(file)
  }

  class ResourceInput(path: String) extends RulesInput {

    private[this] val uri = new URI(path)

    override val srcType: String = "resource"

    override def name: String = uri.toString

    override def relative(path: String): RulesInput =
      new ResourceInput(uri.resolve(path).toString)

    override def inputStream(): InputStream =
      classOf[RulesInput].getClassLoader.getResourceAsStream(uri.toString)
  }
}

trait RulesInput {

  val srcType: String

  def name: String

  def relative(path: String): RulesInput

  def inputStream(): InputStream
}
